import os
import csv
import time
import re




def get_pathways():
	bacs = {"lbr":"lactobacillus_brevis_ATCC_367",
			"ljo":"lactobacillus_johnsonii_NCC_533",
			"lcb":"lactobacillus_casei_BL23",
			"lpl":"lactobacillus_plantarum_WCFS1_RAST",
			"lfr":"lactobacillus_fermentum_CECT_5716",
			"lrf":"lactobacillus_reuteri_JCM_1112",
			"lga":"lactobacillus_gasseri_ATTC_33323",
			"lsi":"lactobacillus_salivarius_CECT_5713",
			"lhl":"lactobacillus_helveticus_H10",
			"lsn":"lactobacillus_sanfranciscensis_TMW_1_1304"}


	for code in bacs.keys():
		print(code)
		os.system("wget http://rest.kegg.jp/list/pathway/{} -O /home/aponnu/genome_reporter/data/pathways_lactobacillus/{}_pathways.txt".format(code, bacs[code]))
				
def collect_unique_pathways():
	
	pathways = ["lactobacillus_brevis_ATCC_367_pathways.txt",
				"lactobacillus_casei_BL23_pathways.txt",
				"lactobacillus_fermentum_CECT_5716_pathways.txt",
				"lactobacillus_gasseri_ATTC_33323_pathways.txt",
				"lactobacillus_helveticus_H10_pathways.txt",
				"lactobacillus_johnsonii_NCC_533_pathways.txt",
				"lactobacillus_plantarum_WCFS1_RAST_pathways.txt",
				"lactobacillus_reuteri_JCM_1112_pathways.txt",
				"lactobacillus_salivarius_CECT_5713_pathways.txt",
				"lactobacillus_sanfranciscensis_TMW_1_1304_pathways.txt"]
	pathwayssingle = ["lactobacillus_brevis_ATCC_367_pathways.txt"]
				
	org_pathways = {}
	
	for orgpathway in pathways:
		with open("/home/aponnu/genome_reporter/data/pathways_lactobacillus/"+orgpathway, "r") as path:
			pathways = []
			for line in path:
				pathwaynumber = line.strip().split("\t")[0][8:]
				pathways.append(pathwaynumber)
		org_pathways[orgpathway] = pathways
	
	
	unique_for_lplantarumWCFS1 = []
	
	for org in org_pathways.keys():
		pathways = []
		if org == "lactobacillus_plantarum_WCFS1_RAST_pathways.txt":
			continue
		else:
			for item in org_pathways[org]:
				if item not in org_pathways["lactobacillus_plantarum_WCFS1_RAST_pathways.txt"]:
					pathways.append(item)
					# unique_for_lplantarumWCFS1.append(item)
			unique_for_lplantarumWCFS1.append({org:pathways})
	uniquepathsways = []
	c = 0
	for ufl in unique_for_lplantarumWCFS1:
		key = ufl.keys()[0]
		uniquepathsways = ufl[key] + uniquepathsways
		# print(uniquepathsways)
		c = c + len(ufl[key])
		print(key + " : " + str(ufl[key]))
	print(sorted(set(uniquepathsways)))
			
def executeWget():
	os.system("wget ftp://ftp.ncbi.nih.gov/genomes/Bacteria/Lactobacillus_casei_BL23_uid59237/NC_010999.ptt -O /home/aponnu/genome_reporter/data/ptt/lactobacillus_casei_BL23.ptt")

def genePTT(pttFile):
	
	pttFileContent = {}
	with open(pttFile, "r") as ptt:

		reader = csv.reader(ptt, delimiter='\t')
		for geneInfo in reader:
			if len(geneInfo) == 9:
				# ['Location', 'Strand', 'Length', 'PID', 'Gene', 'Synonym', 'Code', 'COG', 'Product']
				row = {}
				row["location"] = geneInfo[0]
				# print(geneInfo)
				row["strand"] = geneInfo[1]
				row["length"] = geneInfo[2]
				row["pid"] = geneInfo[3]
				row["gene"] = geneInfo[4]
				row["synonym"] = geneInfo[5]
				row["code"] = geneInfo[6]
				row["cog"] = geneInfo[7]
				row["product"] = geneInfo[8]
				pttFileContent[geneInfo[5]] = row
		# print()
		# print(pttFileContent)
	return pttFileContent

def getGenesFromKEGGFile(outputFileName):
	
	pathwayGenes = []
	with open(outputFileName, "r") as keggPathwayFile:
		reader = csv.reader(keggPathwayFile, delimiter='\t')
		for row in reader:
			
			if len(row) != 0:
				locusTag = row[1].split(":")[1]
				pathwayGenes.append(locusTag)
	return pathwayGenes

def getPathwayGenes(orgCode, pathwayCode):
	
	outputFileName = "./temp/pathway_{}_org_{}.txt".format(pathwayCode, orgCode)
	cmd = "wget http://rest.kegg.jp/link/{}/{}{} -O {} -q".format(orgCode, orgCode, pathwayCode, outputFileName)
	os.system(cmd)
	pathwayGenes = getGenesFromKEGGFile(outputFileName)
	
	return pathwayGenes
	
def getPathwayCluster():
	
	os.system("mkdir temp")
	PTTFilesPaths = os.listdir("/home/aponnu/genome_reporter/data/ptt")
	# os.system("http://rest.kegg.jp/link/lcb/lcb00071")
	#http://rest.kegg.jp/link/lcb/lcb00071
	pathways = ["00071", "00630", "00750"]
	# orgCodes = [ptt.split("_")[0] for ptt in PTTFilesPaths ]
	pathWayGeneCluster = {}
	for pathway in pathways:
		geneSet = []
		for ptt in PTTFilesPaths:
			orgCode = ptt.split("_")[0]
			pttFile =  genePTT("/home/aponnu/genome_reporter/data/ptt/{}".format(ptt))
			# print(pttFile)
			pathwayGenes = getPathwayGenes(orgCode, pathway)
			time.sleep(5)
			for locusTag in pathwayGenes:
				if locusTag in pttFile.keys():
					geneSet.append(pttFile[locusTag]["pid"])
			pathWayGeneCluster[pathway] = geneSet
		
	print(pathWayGeneCluster)
	
	
	
	
	os.system("rm -rf temp")
				
def getOrgPathwayGenes(pathway):
	
	all_pathway_genes = []
	

	PTTFilesPaths = os.listdir("/home/aponnu/genome_reporter/data/ptt")
	c = 0
	for ptt in PTTFilesPaths:
		print(ptt)
		orgCode = ptt.split("_")[0]
		print("")
		pathwayGenes = getPathwayGenes(orgCode, pathway)
		all_pathway_genes = all_pathway_genes+pathwayGenes



	return all_pathway_genes

def writeLocusTagToFile(all_pathway_genes, outputfile):
	print("writing locus tag files.....")
	outputFile = "./temp/pathwayGeneSets/{}.txt".format(outputfile)
	with open(outputFile, "w") as outFile:
		for locustag in all_pathway_genes:
			line = "{}\n".format(locustag)
			outFile.write(line)

def getKEGGFiles():
	keggPathways = {}
	p = "path:map([\d]{5})"
	with open("/home/aponnu/genome_reporter/programming_lib/temp/pathwaysFromKEGG.txt", "r") as keggPathwayFile:
		reader = csv.reader(keggPathwayFile, delimiter='\t')			
		for row in reader:
			pathwaycode = ""
			m = re.match(p, row[0])
			if m:
				pathwaycode = m.group(1)
			pathwayname = row[1]
			keggPathways[pathwaycode] = pathwayname 

	# with open("/home/aponnu/genome_reporter/programming_lib/temp/pathwayCode2pathwayname.txt", "w") as pc2pn:
		# for key in keggPathways.keys():
			# row = "{}\t{}".format(key, keggPathways[key])
			# pc2pn.write(row+"\n")
	c = 0
	for key in keggPathways.keys():
		print("kegg pathway: "+ key)
		outputfile = key
		allPathwatGenes = getOrgPathwayGenes(key)
		writeLocusTagToFile(allPathwatGenes, outputfile)
		if c == 10:
			print("sleeping......")
			time.sleep(3)
			c = 0
		c+=1

def orderHypotheticalProteinFile():
	
	hypo_prot_data = []
	with open("./genome_results/hypothetical_prot_annotation_OrthoMCL_id.txt", "r") as hpa:
		
		reader = csv.reader(hpa, delimiter="\t")
		for hp in reader:
			start, stop = hp[1].split("..")
			hp[1] = (int(start), int(stop))
			
			hypo_prot_data.append(hp)
			
	hypo_prot_data = sorted(hypo_prot_data, key=lambda e: e[1][0], reverse=False)
	c=1
	for hpd in hypo_prot_data:
		# orths_joined = "\t".join(str(orth) for orth in hpd[2:] if orth != "")  
		# print("{}\t{}..{}\t{}".format(hpd[0], hpd[1][0], hpd[1][1], orths_joined))
		if len(hpd) >= 7:
			c+=1
			orths_joined = "\t".join(str(orth) for orth in hpd[2:] if orth != "")  
			print("{}\t{}..{}\t{}".format(hpd[0], hpd[1][0], hpd[1][1], orths_joined))

orderHypotheticalProteinFile()
		
		
		