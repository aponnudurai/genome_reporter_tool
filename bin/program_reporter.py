from GenBank import Genbank
from ReportFactory import Reportfactory
from MyDocTemplate import MyDocTemplate
import config
from reportlab.lib.pagesizes import letter, A4, inch
from reportlab.platypus.doctemplate import NextPageTemplate
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table, TableStyle, PageBreak
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.lib import colors 


def main():
	
	doc = MyDocTemplate(config.pdfName)
	rf = Reportfactory(config.output)	
	g = Genbank(config.genbank)
	
	cover = rf.getCoverElements(config.logo, g.getGenomeName(), config.circulair_genome_image)
	
	# Make cover of report
	if config.cover:
		print("Cover report")
		rf.addElements(cover["logo"])
		rf.addElements(Spacer(1, 12))
		rf.addElements(cover["title"])
		rf.addElements(Spacer(1, 24))
		rf.addElements(cover["cgi"])
		rf.addElements(Spacer(1, 48))
		rf.addElements(cover["date"])
		rf.addElements(NextPageTemplate("normal"))
		rf.addElements(PageBreak())
		rf.addElements(rf.getTableOfContentsElement())
		rf.addElements(Spacer(1, 48))
		rf.addElements(PageBreak())
	
	# Make introduction and material & methods
	if config.mm:
		print("Introduction and Material and methods")
		rf.addElements(rf.getHeader1Element("Introduction"))
		rf.addElements(Spacer(1, 48))	
		rf.addElements(rf.getInformativeText(config.intro_text))
		rf.addElements(PageBreak())
		rf.addElements(rf.getHeader1Element("Materials and methods"))
		rf.addElements(Spacer(1, 48))
		rf.addElements(rf.getInformativeText(config.M_and_M_text))
		rf.addElements(PageBreak())
	
	# Make genomic properties chapter	
	if config.genomic_properties:
		print("Genomic properties")
		rf.addElements(rf.getHeader1Element("Genomic properties"))
		rf.addElements(Spacer(1, 12))
		rf.addElements(rf.getInformativeText(config.genomic_properties_txt))
		rf.addElements(Spacer(1, 12))	
		rf.addElements(rf.getGenbankStats(g, config.fasta))
		rf.addElements(rf.getTableDescription("Table 1: Sequence characteristics of the provided genome."))
		rf.addElements(Spacer(1, 24))
		rf.addElements(rf.getInformativeText(config.codon_usage_txt))
		rf.addElements(Spacer(1, 12))
	
	# Make codon usage table and table with genes with most deviating CAI
	if config.diff_codon_usage:
		print("Genes codon usage")
		rf.addElements(rf.getCodonUsage(g, config.fasta))
		rf.addElements(rf.getTableDescription("Table 2: Codon usage of the provided genome."))
		rf.addElements(PageBreak())
		rf.addElements(rf.getInformativeText(config.genes_with_bias_codon_usage_txt))
		rf.addElements(Spacer(1, 24))
		rf.addElements(rf.getGenesWithBiasedCodonUsage(g, config.fasta, config.complete_cai))
		rf.addElements(rf.getTableDescription("Table 3: Genes with most deviating codon usage."))
		rf.addElements(Spacer(1, 12))
		rf.addElements(rf.getParagraphElement("To see the complete table click <a href=\"pdf:{}\"><i>here</i>.</a>".format(config.complete_cai)))
		rf.addElements(Spacer(1, 12))
	
	# Make table with genes with most deviating GC
	if config.diff_gc_content:
		print("Genes GC table")
		rf.addElements(PageBreak())
		rf.addElements(rf.getInformativeText(config.genes_with_significant_GC_percentage))
		rf.addElements(Spacer(1, 24))
		rf.addElements(rf.getGenesWithBiasedGCPrecentage(g, config.fasta, config.complete_gc))
		rf.addElements(rf.getTableDescription("Table 4: Genes with most deviating GC percentage."))
		rf.addElements(Spacer(1, 12))
		rf.addElements(rf.getParagraphElement("To see the complete table click <a href=\"pdf:{}\"><i>here</i>.</a>".format(config.complete_gc)))
		rf.addElements(PageBreak())
	
	# Make table with hypothetical protein annotation 
	if config.hypothetical_annotation:
		print("Hypothetical proteins")
		rf.addElements(rf.getHeader1Element("Orthologous genes"))
		rf.addElements(Spacer(1, 12))
		rf.addElements(rf.getInformativeText(config.orthologous_genes_txt))	
		rf.addElements(Spacer(1, 24))
		rf.addElements(rf.getHypotheticalProteinAnnotationTable(config.IDs_to_tag, config.groups, config.orthoMCL_query_genome_id, config.complete_orthologs))
		rf.addElements(rf.getTableDescription("Table 5: Gene products of the provided genome and its orthologs from the reference genomes."))
		rf.addElements(Spacer(1, 12))
		rf.addElements(rf.getParagraphElement("To see the complete table with orthologs click <a href=\"pdf:genomes_ortho_table.pdf\"><i>here</i>.</a>"))
		rf.addElements(PageBreak())
	
	# Make table with potential metabolic pathways 
	if config.metabolic_pathways:
		print("Metabolic pathways")
		rf.addElements(rf.getHeader1Element("Metabolic pathways in the genome"))
		rf.addElements(Spacer(1, 12))
		rf.addElements(rf.getInformativeText(config.metabolic_pathway_text))	
		rf.addElements(Spacer(1, 24))
		rf.addElements(rf.getMetabolicPathwayTable(g, config.pathway_geneset_folder, config.IDs_to_tag, config.groups, config.pttFolder, config.orthoMCL_query_genome_id, config.pathwayCode2PathwayName, config.complete_pathways))
		rf.addElements(rf.getTableDescription("Table 6: Potentail metabolic pathways."))
		rf.addElements(Spacer(1, 12))
		rf.addElements(rf.getParagraphElement("To see the complete table with pathways click <a href=\"pdf:genome_pathways.pdf\"><i>here</i></a>. "))

	
	
	if config.build:
		print("doc....")
		doc.multiBuild(rf.elements)
		print("build done...")
	
if __name__ == '__main__':
	main()
