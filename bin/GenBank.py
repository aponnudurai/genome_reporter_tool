#!/usr/bin/python	
"""
GenBank
---------------------
.. module:: GenBank

:synopsis:

.. moduleauthor:: Ashvin Ponnudurai<Ashvin.Ponnudurai@nizo.com>

|  
"""
from Bio import SeqIO
from Bio.Graphics import GenomeDiagram
from reportlab.lib import colors
from reportlab.lib.units import cm, inch
from reportlab.platypus import Table, TableStyle, Image
import numpy

class Genbank():
	'''
	annotations
	dbxrefs
	description
	features
	format
	id
	letter_annotations
	lower
	name
	reverse_complement
	seq
	upper
	None
	'''
	
	def __init__(self, file_name):
		self.file_name = file_name
		self.genbank = SeqIO.read(file_name, "genbank")
	
	def getGeneTable(self, number_of_genes):
		'''
		comments
		'''
		
	def getIntergenicRegion(self):
		'''
		comments
		'''
		
		deviations = []
		gene_stop = ""
		counter = 0
		avg = 0
		for feature in self.genbank.features:
			if feature.type == "CDS":
				counter += 1
				if counter == 1:
					gene_stop = feature.location.end
					continue
				elif gene_stop != "":
					fls = feature.location.start
					dev = fls - gene_stop
					gene_stop = feature.location.end
					
					if dev > 0: 
						deviations.append(dev)
		
		avg = int(round(numpy.mean(deviations)))
		return avg
		
	def  getAverageGeneLength(self):
		'''
		comments
		'''
		gene_lengts = []
		for feature in self.genbank.features:
			if feature.type == "CDS":
				gene_lengts.append((feature.location.end - feature.location.start))
		avg = int(round(numpy.mean(gene_lengts)))
		
		return avg
		
	def getGenes(self):
		'''
		comments
		'''
		
		all_genes = []
		# all_genes = [i for i in self.genbank.features if i.type == "gene"]
		for feature in self.genbank.features:
			if feature.type == "CDS":
				# print(feature.qualifiers)
				all_genes.append(feature)
		
		
		return all_genes
	
	def getNumberOfGenes(self):
		'''
		comments
		'''
		number_of_genes = len([i for i in self.genbank.features if i.type == "CDS"])
		
		return str(number_of_genes)

	def getGenomeName(self):
		'''
		comments
		'''
		return self.genbank.description
	
	def getGenomeSize(self):
		'''
		comments
		'''
		source = [i for i in self.genbank.features if i.type == "source"][0]
		return str(source.location.end)
	
	def getGene(self, locus_tag):
		'''
		comments
		'''
		for feature in self.genbank.features:
			if feature.type == "CDS":
				for feature_locus_tag in feature.qualifiers["locus_tag"]:
					if feature_locus_tag == locus_tag:
						return feature
	
	
	def getGeneBasedOnLocation(self, start, end):
		'''
		comments
		'''
		
		for feature in self.genbank.features:
			if int(feature.location.start) == start and int(feature.location.end) == end:
				return feature

	
	def __str__(self):

		return str(dir(self))
	
	
	
gb = Genbank("/home/aponnu/genome_reporter/data/Lactococcus_lactis_subsp_cremoris_A76_complete_genome.gb")	
# gb = Genbank("/home/aponnu/genome_reporter/data/Streptococcus_pyogenes_MGAS315_complete_genome.gb")	
gb.getAverageGeneLength()

	
	
	
	
		