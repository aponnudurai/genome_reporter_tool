from __future__ import division
from reportlab.lib.pagesizes import  A4, inch
from reportlab.platypus import Paragraph, Spacer, Image, Table, TableStyle
from reportlab.platypus.tableofcontents import TableOfContents
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch, cm
from reportlab.lib import colors 
from GenBank import Genbank
from PDFtable import PDFtable
from Bio.Graphics import GenomeDiagram
from Bio import SeqIO
from operator import itemgetter
from hashlib import sha1
from Bio.SeqUtils import CodonUsage 
import os, math, re, datetime, time, csv, numpy




class Reportfactory:
	'''
	comments
	'''

	def __init__(self, output):
		'''
		comments
		'''
		self.elements = []
		self.styles = getSampleStyleSheet()
		self.output = output
			
	def getCoverElements(self, logo_path, title, circular_genome):
		'''
		comments
		'''
		cover_elements = {}
		
		cover_elements["title"] = self.getParagraphElement(title, "title")
		
		logo = Image(logo_path, hAlign="RIGHT")
		logo.drawHeight = 0.5*inch
		logo.drawWidth = 1*inch
		cover_elements["logo"] = logo
		
		i = datetime.datetime.now()
		date = "<b>{} {} {}</b>".format(i.day, time.strftime("%B"), i.year )
		p = ParagraphStyle("my_style")
		p.alignment = 1
		paragraph = Paragraph(date, p)
		cover_elements["date"] = paragraph
		
		circular_genome_image = Image(circular_genome, hAlign="CENTER")
		circular_genome_image.drawHeight = 6*inch
		circular_genome_image.drawWidth = 6*inch
		cover_elements["cgi"]  = circular_genome_image

		return cover_elements
	
	def getHeader1Element(self, header):
		'''
		comments
		'''
		h1 = ParagraphStyle(name = "Heading1", fontSize=18, leading=16, fontName='Helvetica-Bold')
		bn=sha1(header+h1.name).hexdigest()
		
		#modify paragraph text to include an anchor point with name bn
		h=Paragraph(header+'<a name="{}"/>'.format(bn), h1) 
		
		#store the bookmark name on the flowable so afterFlowable can see this
		h._bookmarkName=bn
		h_test = Paragraph(header, h1)
		
		
		
		return h
	
	def getHeader2Element(self, header):
		'''
		comments
		'''
		h2 = ParagraphStyle(name = "Heading1", fontSize=12, leading=14, fontName='Helvetica')
		
		paragraph = Paragraph(header, h2)
		
		return paragraph
	
	def getGenbankStats(self, genbank, fasta):
		'''
		Note:
			Makes table with genome sequence statistics.
			
		Args:
			| genbank: genbank file of query genome.
			| fasta: fasta file from query genome.
		
		Returns:
			table, reportlab.platypus.table object with sequence statistics.
		'''
		
		fasta_genome = SeqIO.read(fasta, "fasta")
		gc_per = round(((fasta_genome.seq.count("C") + fasta_genome.seq.count("G")) / len(fasta_genome.seq))*100, 1)
		
		data = []
		data.append(["Genome size", Paragraph(str(genbank.getGenomeSize()) + " bp", self.styles["Normal"])])
		data.append(["Number of genes", genbank.getNumberOfGenes()])
		data.append(["Average gene length", Paragraph(str(genbank.getAverageGeneLength()) + " bp", self.styles["Normal"])])
		data.append(["Average intergenic distance", Paragraph(str(genbank.getIntergenicRegion()) + " bp", self.styles["Normal"])])
		data.append(["GC percentage", Paragraph(str(gc_per) + " %", self.styles["Normal"])])
		
		numb_of_rows = len(data)
		TableStyle = []
		for  row_nmb in range(0, numb_of_rows):
			# print(row_nmb)
			if row_nmb % 2 == 0:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), colors.white))
			else:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), "#EAFFFF"))
			TableStyle.append(("VALIGN", (0,row_nmb),(-1,row_nmb), "TOP"))
		table = Table(data, 2*[5*cm], hAlign="LEFT")
		TableStyle.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
		TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
		
		table.setStyle(TableStyle)
		return table
		
	def getGenesWithBiasedGCPrecentage(self, genbank, fasta, complete_gc):
		'''
		Note:
			Makes table with genes with most deviating GC and makes a PDF report with GC value of all protein coding genes.
			
		Args:
			| genbank: genbank file of query genome.
			| fasta: fasta file from query genome.
		
		Returns:
			table, reportlab.platypus.table object with ten genes with most deviating GC.
		'''
		
		# genome_orfs_file = self.writeORFGenomeFile(genbank, fasta)
		# fasta_genome = list(SeqIO.parse(genome_orfs_file, "fasta"))
		# genesGCPercentages = self.caluculateGeneGCPercentage(fasta_genome)
		GCPercentagesValues = []
		
		
		p_style = ParagraphStyle("my_style")
		p_style.leading = 9
		data = []
		
		complete_data = []
		fasta_genome = SeqIO.read(fasta, "fasta")
		for feature in genbank.getGenes():
			row = []
			locus_tag = "."
			product = "."
			gene_name = "."
			gene_location = "."
			gc_per = "."
			
			if "locus_tag" in feature.qualifiers.keys():
				locus_tag = feature.qualifiers["locus_tag"][0]
			locus_tag_NCBI_link = "<link href='http://www.ncbi.nlm.nih.gov/gene/?term={}'>{}</link>".format(locus_tag, locus_tag)
			row.append(Paragraph(locus_tag_NCBI_link, p_style))
			
			if "gene" in feature.qualifiers.keys():
				gene_name = feature.qualifiers["gene"][0]
			gene_name_NCBI_link = "<link href='http://www.ncbi.nlm.nih.gov/gene/?term={}'>{}</link>".format(gene_name, gene_name)
			row.append(Paragraph(gene_name_NCBI_link, p_style))
			
			if "product" in feature.qualifiers.keys():
				product = feature.qualifiers["product"][0]
			row.append(Paragraph(product, p_style))
			
			gene_location = (int(feature.location.start), int(feature.location.end))
			row.append(gene_location)
			
			orf_seq = str(fasta_genome.seq[feature.location.start:feature.location.end])
			gc_per = self.caluculateGeneGCPercentage(orf_seq)
			
			row.append(round(gc_per, 2))
			GCPercentagesValues.append(round(gc_per, 2))
			complete_data.append(row)
		
		sorted_data = sorted(complete_data, key=lambda e: e[3][0], reverse=False)
		for i in sorted_data:
			i[3] = "{}...{}".format(i[3][0], i[3][1])

		if os.path.isfile(complete_gc):
			pass
		else:
			sorted_data.insert(0, ["Locus tag", "Gene", "Product", "Location", "GC"])
			table_style_complete =  self.addBgTable(len(sorted_data))
			table_style_complete.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
			table_style_complete.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
			table_complete = Table(sorted_data, colWidths=(2*cm, 2*cm, 8.5*cm, 3.5*cm, 2*cm),hAlign="LEFT")
			table_complete.setStyle(table_style_complete)
			pdftable = PDFtable(complete_gc)
			pdftable.addTableElement(table_complete)
			pdftable.savePDF()
			print("genomes_genes_GC_content.pdf saved......")
		
		mean = numpy.mean(GCPercentagesValues)
		sd = numpy.std(GCPercentagesValues)
		zscores = {}
		sd_value = 1
		
		for index, row in enumerate(complete_data):
			if type(row[4]) == type(""):
				pass
			else:
				zscores[index] = ((int(row[4])-mean/sd))
			
		higherThanZero = [zs for zs in zscores.values() if zs > 0]
		lessThanZero = [zs for zs in zscores.values() if zs < 0]
		
		zscores = sorted(zscores.items(), key=itemgetter(1))
		selected_genes = []
		if higherThanZero > lessThanZero:
			selected_genes = zscores[:10]
		elif higherThanZero == lessThanZero:
			selected_genes = zscores[:10] + zscores[-10:]
		elif higherThanZero < lessThanZero:
			selected_genes = zscores[-10:]
		
		partion_data = [complete_data[tup[0]] for tup in selected_genes]
		
		partion_data.insert(0, ["Locus tag", "Gene", "Product", "Location", "GC"])
		TableStyle =  self.addBgTable(len(partion_data))
		table = Table(partion_data, hAlign="LEFT")
		TableStyle.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
		TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
		table.setStyle(TableStyle)
		
		
		
		return table
		
	def caluculateGeneGCPercentage(self, orf_seq):
		'''
		comments
		'''
		
		gc_per = ((orf_seq.count("C") + orf_seq.count("G")) / len(orf_seq))*100
			
		return gc_per
		
	def getGenesWithBiasedCodonUsage(self, genbank, fasta, complete_cai):
		'''		
		Note:
			Makes table with genes with most deviating CAI and makes a PDF report with CAI of all protein coding genes
			
		Args:
			| genbank: genbank file of query genome.
			| fasta: fasta file from query genome.
		
		Returns:
			table_part, reportlab.platypus.table object with genes with most deviating CAI.
		'''

		genome_orfs_file = self.writeORFGenomeFile(genbank, fasta)
		
		cai = CodonUsage.CodonAdaptationIndex()
		
		cai.generate_index(genome_orfs_file)
		
		p_style = ParagraphStyle("my_style")
		p_style.leading = 9
		
		ref_cai_indices = []
	
		complete_data = []
		fasta_genome = SeqIO.read(fasta, "fasta")
		for feature in genbank.getGenes()[:200]:
			row = []
			orf_info = {}
			locus_tag = "."
			product = "."
			gene_name = "."
			gene_location = "."
			gc_per = "."
			
			if "locus_tag" in feature.qualifiers.keys():
				locus_tag = feature.qualifiers["locus_tag"][0]
			locus_tag_NCBI_link = "<link href='http://www.ncbi.nlm.nih.gov/gene/?term={}'>{}</link>".format(locus_tag, locus_tag)
			row.append(Paragraph(locus_tag_NCBI_link, p_style))
			
			if "gene" in feature.qualifiers.keys():
				gene_name = feature.qualifiers["gene"][0]
			gene_name_NCBI_link = "<link href='http://www.ncbi.nlm.nih.gov/gene/?term={}'>{}</link>".format(gene_name, gene_name)
			row.append(Paragraph(gene_name_NCBI_link, p_style))
			
			if "product" in feature.qualifiers.keys():
				product = feature.qualifiers["product"][0]
			row.append(Paragraph(product, p_style))
			
			gene_location = (int(feature.location.start), int(feature.location.end))
			row.append(gene_location)
			
			orf_seq = str(fasta_genome.seq[feature.location.start:feature.location.end])
			orf_cai = cai.cai_for_gene(orf_seq)
			row.append(round(orf_cai, 2))
		
			complete_data.append(row)
		
		sorted_data = sorted(complete_data, key=lambda e: e[3][0], reverse=False)
		
		for i in sorted_data:
			i[3] = "{}...{}".format(i[3][0], i[3][1])
		
		if os.path.isfile(complete_cai):
			pass
		else:
			sorted_data.insert(0, ["Locus tag", "Gene", "Product", "Location", "CAI"])
			table_style_complete =  self.addBgTable(len(sorted_data))
			table_style_complete.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
			table_style_complete.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
			table_complete = Table(sorted_data, hAlign="CENTER")
			table_complete.setStyle(table_style_complete)
			pdftable = PDFtable(complete_cai)
			pdftable.addTableElement(table_complete)
			pdftable.savePDF()
			print(complete_cai+" saved......")
		
		cai_values = []
		for row in complete_data:
			cai_values.append(row[4])
		
		mean = numpy.mean(cai_values)
		median = numpy.median(cai_values)
		sd = numpy.std(cai_values)
	
		data = []
		upper_limit_cai = {}
		lower_limit_cai = {}
		
		for index, row in enumerate(sorted_data):
			if index != 0:
				if ((row[4]-mean)/sd) < -1.89:
					lower_limit_cai[index] = ((row[4]-mean)/sd)
				if ((row[4]-mean)/sd) > 1.89:
					upper_limit_cai[index] = ((row[4]-mean)/sd)
		
		lower_cai_indices = sorted(lower_limit_cai, key=lambda e: lower_limit_cai[e])[:5]
		upper_cai_indices = sorted(upper_limit_cai, key=lambda e: upper_limit_cai[e])[-5:]
		
		significant_dif_codon_usage_orfs_index = lower_cai_indices + upper_cai_indices
		
		sorted(significant_dif_codon_usage_orfs_index)
		part_data_table = [sorted_data[index] for index in significant_dif_codon_usage_orfs_index]
		
		
		part_data_table.insert(0, ["Locus tag", "Gene", "Product", "Location", "CAI"])
		table_style_part =  self.addBgTable(len(part_data_table))
		table_style_part.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
		table_style_part.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
		table_part = Table(part_data_table, hAlign="CENTER")
		table_part.setStyle(table_style_part)
		

		

		return table_part
		
	def getCodonUsage(self, genbank, fasta):
		'''
		Note:
			Makes codong usage table.
			
		Args:
			| genbank: genbank file of query genome.
			| fasta: fasta file from query genome.
		
		Returns:
			table, reportlab.platypus.table object with codon usage.
		'''
		table = None
		codon_count_orf_list = []
		
		genome_orfs_file = self.writeORFGenomeFile(genbank, fasta)
		
		genome_codon_usage = self.calculateCodonUsage(genome_orfs_file)

		data = []
		row_value = []
		row_length = 8
		for aminoacid_codon_fam in genome_codon_usage.keys():
			# print(aminoacid_codon_fam)
			codon_fam_freq = "<font size=8>"
			for codon in genome_codon_usage[aminoacid_codon_fam].keys():
				codon_fam_freq = codon_fam_freq + codon + "  " + str(genome_codon_usage[aminoacid_codon_fam][codon]) + "<br />"
				# print(codon_fam_freq)
			codon_fam_freq = codon_fam_freq + "</font>"
			
			aminoacid_codon_freq = "<font size=8>{}</font>".format(aminoacid_codon_fam) + "<br /><br />" + codon_fam_freq
			

			# p = Paragraph(codon_fam_freq, self.styles["Normal"])
			# p_amino_codon_fam = Paragraph("<font size=10>{}</font>".format(aminoacid_codon_fam), self.styles["Normal"])
			p = Paragraph(aminoacid_codon_freq, self.styles["Normal"])
			# row_value.append(p_amino_codon_fam)
			row_value.append(p)
			
			
			if genome_codon_usage.keys().index(aminoacid_codon_fam) == len(genome_codon_usage.keys()) - 1:
				for i in range(0, row_length-2):
					row_value.append("")
	
			if len(row_value) == row_length:
				data.append(row_value)	
				row_value = []
		
		
		
		TableStyle = []
		
		numb_of_rows = len(data)
		for  row_nmb in range(0, numb_of_rows):
			# print(row_nmb)
			if row_nmb % 2 == 0:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), colors.white))
			else:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), "#EAFFFF"))
			TableStyle.append(("VALIGN", (0,row_nmb),(-1,row_nmb), "TOP"))
		
		
		table = Table(data, hAlign="LEFT")
		TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
		TableStyle.append(('VALIGN',(0,0),(-1,-1),'TOP'))
		TableStyle.append(("INNERGRID", (2,-1),(-1,-1), 0.25, colors.white))
		table.setStyle(TableStyle)
		
		return table
		
	def calculateCodonUsage(self, genome_orfs_file):
		'''
		comments
		'''
		
		sc = CodonUsage.SynonymousCodons
		codon_usage_genome = {}
		
		cai = CodonUsage.CodonAdaptationIndex()
		cai._count_codons(genome_orfs_file)
		# print(cai.codon_count)
		# print(sc)
		
		
		for aminoacid in sc.keys():
			codon_counts = {}
			for codon in sc[aminoacid]:
				codon_counts[codon] = cai.codon_count[codon]
			codon_usage_genome[aminoacid] = codon_counts
		
		codon_usage_percentage = {}
		# calculate codon frequency
		for aminoacid in codon_usage_genome.keys():
			total_codons = sum(codon_usage_genome[aminoacid].values())
			codon_distribution = {}
			codons_usages = ""
			# print(aminoacid)
			for codon in codon_usage_genome[aminoacid].keys():
				percentage = 0
				if codon_usage_genome[aminoacid][codon] != 0:
					percentage = round((codon_usage_genome[aminoacid][codon] / total_codons)*100)
				codon_distribution[codon]  = percentage
			codon_usage_percentage[aminoacid] = codon_distribution
		
		return codon_usage_percentage
	
	def writeORFGenomeFile(self, genbank, fasta):
		'''
		comments
		'''
		fasta_genome = SeqIO.read(fasta, "fasta")
		genome_orfs_file = "orfs_genomes.fasta"
		orf_header = ""
		with open(genome_orfs_file, "w") as file:			
			
			
			for feature in genbank.getGenes():
				# print(dir(feature.location))
				
				# print(feature.location.end)
				if "locus_tag" in feature.qualifiers.keys():
					orf_header = feature.qualifiers["locus_tag"][0]
				
				else:
					
					orf_header = "{}...{}".format(feature.location.start, feature.location.end)
	
				orf = fasta_genome.seq[feature.location.start:feature.location.end]
				file.write(">"+orf_header+"\n")
				file.write(str(orf)+"\n")
		
		
		return genome_orfs_file
	
	def getParagraphElement(self, text, style="Normal"):
		'''
		comments
		"BodyText", "Bullet", "Code", "Definition", "Heading1"
		'''
		
		# custom_style = ParagraphStyle(name="own_style", bulletFontName = "Verdana", fontSize = 18)
		
		paragraph = Paragraph(text, self.styles[style])
		
		return paragraph
	
	def getGenbankImageElement(self, genbank, number_of_genes, gene_map_name):
		'''
		comments
		'''
		
		gd_diagram = GenomeDiagram.Diagram(genbank.getGenomeName())
		gd_track_for_features = gd_diagram.new_track(1, name="Annotated Features")
		gd_feature_set = gd_track_for_features.new_set()

		all_genes = genbank.getGenes()
		# c = 0
		# l = []
		# for gene in all_genes:
			# if "gene" in gene.qualifiers.keys():
				# l.append(gene)
	
		for feature in all_genes:
			
			if len(gd_feature_set) % 2 == 0:
				color = colors.blue
			else:
				color = colors.lightblue

			gd_feature_set.add_feature(feature, color=color, label=True, label_size=20)
		
		gd_diagram.draw(format="circular", circular=True, pagesize=(30*cm,30*cm), start=0, end=all_genes[-1].location.end, circle_core=0.5)
		
		# gd_diagram.draw(format="circular", circular=True, pagesize=(20*cm,20*cm),start=0, end=all_genes[-1].location.end, circle_core=0.7)

		gd_diagram.write(str(gene_map_name), str("PNG"))
		
		im = Image(gene_map_name, 3*inch, 3*inch, hAlign="LEFT")
		return im
	
	def getTableOfContentsElement(self):
		'''
		comments
		'''
		toc = TableOfContents()
		toc.levelStyles = [ParagraphStyle(fontName='Times-Bold', fontSize=20, name='TOCHeading1', leftIndent=20, firstLineIndent=-20, spaceBefore=10, leading=16),
		ParagraphStyle(fontName='Times', fontSize=18, name='TOCHeading2', leftIndent=40, firstLineIndent=-20, spaceBefore=5, leading=12),]
		return toc
	
	def getInformativeText(self, intro_file):
		'''
		comments
		'''
		txt = ""
		with open(intro_file, "r") as file:
			for line in file:
				txt = txt + line
		p = ParagraphStyle("my_style")
		p.alignment = 4
		
		paragraph = Paragraph(txt, p)
		
		return paragraph
			
	def getHypotheticalProteinAnnotationTable(self, IDs_to_tag, groups, orthoMCL_query_genome_id, complete_orthologs):
		'''
		Note:
			Makes table with annotations of hypothetical proteins and makes a PDF report with annotation of all proteins.
			
		Args:
			| genbank: genbank file of query genome.
			| fasta: fasta file from query genome.
			| IDs_to_tag: ortholoMCL id linked to protein.
			| groups: orthologous groups.
			| orthoMCL_query_genome_id: OrthoMCL id of query genome.
			| 
		
		Returns:
			table, reportlab.platypus.table object with hypothetical protein annotation.
		'''
		not_annotiated_genes = []
		id_tags_dict = None
		not_annotiated_orthomcl_id = None
		not_annotiated_gene_orthologous = None
		
		
		id_tags_dict = self.getIdTagsDict(IDs_to_tag)
		not_annotiated_orthomcl_id = self.getQueryGenomeNotAnnotiatedOrthoMCLIdS(id_tags_dict, orthoMCL_query_genome_id)
		not_annotiated_gene_orthologous = self.getNotAnnotiatedOrthologous(not_annotiated_orthomcl_id, groups)
		
		
		if os.path.isfile(complete_orthologs):
			pass
		else:
			custom_style = self.styles["Normal"]
			custom_style.leading = 9
			data = []
			for key in not_annotiated_gene_orthologous.keys():
				row = []
				orthologs = ""
				for orth in not_annotiated_gene_orthologous[key]:
					if str(id_tags_dict[orth]["product"]) == "":
						continue
					else:	
						orthologs += "- " + id_tags_dict[orth]["product"] + "<br />"
				row.append(Paragraph(id_tags_dict[key]["product"],custom_style))
				row.append(Paragraph(orthologs, custom_style))
				data.append(row)
			TableStyle = []
			table_complete = Table(data, hAlign="LEFT")
			TableStyle.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
			TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
			table_complete.setStyle(TableStyle)
			pdftable = PDFtable(complete_orthologs)
			pdftable.addTableElement(table_complete)
			pdftable.savePDF()
			print(complete_orthologs + " saved......")
		
		p = ".*(hypothetical\sprotein).*"
		# p = ".*(putative).*"
		custom_style = self.styles["Normal"]
		custom_style.leading = 9
		custom_style.fontSize = 8
		data = []

		for key in not_annotiated_gene_orthologous.keys():
			row = []
			orthologs = ""
			orthologs_orth_mcl_id = ""
			product = id_tags_dict[key]["product"]
			m = re.match(p, product)
			if m:
				hypoCheck = 0
				for orth in not_annotiated_gene_orthologous[key]:
					refProduct = str(id_tags_dict[orth]["product"])
					if refProduct == "":
						continue
					elif re.match(p, refProduct):
						continue
					else:	
						orthologs += "- " + id_tags_dict[orth]["product"] + "<br />"
						orthologs_orth_mcl_id += "- "+orth+"<br />"
						hypoCheck = 1
				
				if hypoCheck == 1:
					row.append(Paragraph(id_tags_dict[key]["product"],custom_style))
					row.append(Paragraph(orthologs, custom_style))
					data.append(row)
					
		
		data.insert(0, ["Genes", "Orthologs"])
		data = data[:15]
		numb_of_rows = len(data)
		TableStyle = []
		for row_nmb in range(0, numb_of_rows):
			# print(row_nmb)
			if row_nmb % 2 == 0:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), colors.white))
			else:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), "#EAFFFF"))
			TableStyle.append(("VALIGN", (0,row_nmb),(-1,row_nmb), "TOP"))	
		# table = Table(data, colWidths=(4*cm, 2*cm, 4*cm, 4*cm, 4*cm), hAlign="LEFT")
		table = Table(data, hAlign="LEFT")
		TableStyle.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
		TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
		
		table.setStyle(TableStyle)	
		return table
	
	def getMetabolicPathwayTable(self, genbank, pathway_geneset_folder, IDs_to_tag, groups, ptt_folder, orthoMCL_query_genome_id, pathwayCode2PathwayName, complete_pathways):
		'''
		Note:
			Makes table with potential metabolic pathways and makes a PDF with all metabolic pathway which were present in reference genomes.
			
		Args:
			| genbank: genbank file of query genome.
			| pathway_geneset_folder: folder with all metabolic pathway genes per metabolic pathway.
			| IDs_to_tag: ortholoMCL id linked to protein.
			| groups: orthologous groups.
			| ptt_folder: folder with protein translation table of reference genomes.
			| orthoMCL_query_genome_id: OrthoMCL id of query genome.
			| pathwayCode2PathwayName: file with KEGG pathway code linked to pathway name.
			| complete_pathways: file with all potential metabolic pathways.
		
		Returns:
			table, reportlab.platypus.table object with subset of potential metabolic pathways.
		'''
	
		limit = 20
		#Get protein translations
		pc2pn_dict = {}
		pc2pn_handle = open(pathwayCode2PathwayName, "r")
		for line in pc2pn_handle:
			line = line.split("\t")
			pc2pn_dict[line[0]] = line[1].strip()

		pttFilesContent = self.getProteinTranslationTable(ptt_folder)
		result_dict = None
		gene_sets = os.listdir(pathway_geneset_folder)
		
		
		results = {}
		empty_result_dict = {'genes_in_query_org': 0, 'all_meta_genes': 0, 'per_genes_in_query_org': 0, 'additional_genes_in_query_org': 0}
		id_tags_dict = self.getIdTagsDict(IDs_to_tag)

		
		for gene_set in gene_sets:
			pathwayCode = gene_set.split(".")[0]
			gene_set = pathway_geneset_folder+gene_set
			handle = open(gene_set, "r")
			locus_tags = [locus_tag.strip() for locus_tag in handle.readlines()]
			
			if len(locus_tags) == 0:
				results[pc2pn_dict[pathwayCode]] = empty_result_dict
			else:	
				gene_set_orthomcl_ids = self.getGeneSetOrthoMCLIds(id_tags_dict, locus_tags, orthoMCL_query_genome_id)		
				checked_gene_set = self.checkOrthologsPresenceInQueryGenome(gene_set_orthomcl_ids, orthoMCL_query_genome_id, groups)
				result_dict = self.pathwayPresenceInQueryGenome(checked_gene_set, orthoMCL_query_genome_id)
				results[pc2pn_dict[pathwayCode]] = result_dict
	
		
		p_style = ParagraphStyle("my_style")
		p_style.leading = 9
		p_style.fontSize = 8
		
		data = []
		data_present_pathways = []
		
		for key in results.keys():	
			row = []
			pathway_name = key
			genes_in_query_org = str(results[key]["genes_in_query_org"])
			all_meta_genes = str(results[key]["all_meta_genes"])
			per_genes_in_query_org = str(results[key]["per_genes_in_query_org"])
			additional_genes_in_query_org = str(results[key]["additional_genes_in_query_org"])
			row.append(Paragraph(pathway_name, p_style))
			row.append(Paragraph(genes_in_query_org, p_style))
			row.append(Paragraph(all_meta_genes, p_style))
			row.append(Paragraph(per_genes_in_query_org, p_style))
			row.append(Paragraph(additional_genes_in_query_org, p_style))
			# print("{}\t{}\t{}\t{}\t{}".format(pathway_name, genes_in_query_org, all_meta_genes, per_genes_in_query_org, additional_genes_in_query_org))
			data.append(row)
			if results[key].values() != [0] * len(results[key].values()):
				data_present_pathways.append(row)
			
		data.insert(0, [Paragraph("Pathway", p_style), Paragraph("Genes genome", p_style), Paragraph("All potential genes", p_style), Paragraph("Score ", p_style), Paragraph("Additional genes in genome", p_style)])
		numb_of_rows = len(data)
		TableStyle = []
		
		for  row_nmb in range(0, numb_of_rows):
			if row_nmb % 2 == 0:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), colors.white))
			else:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), "#EAFFFF"))
			TableStyle.append(("VALIGN", (0,row_nmb),(-1,row_nmb), "TOP"))
		
		if os.path.isfile(complete_pathways):
			pass
		else:
			table_complete = Table(data, hAlign="LEFT")
			TableStyle.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
			TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
			table_complete.setStyle(TableStyle)
			pdftable = PDFtable(complete_pathways)
			pdftable.addTableElement(table_complete)
			pdftable.savePDF()
			print(complete_pathways + " saved......")
	
		data_present_pathways.insert(0, [Paragraph("Pathway", p_style), Paragraph("Genes genome", p_style), Paragraph("All potential genes", p_style), Paragraph("Percentage of pathway genes", p_style), Paragraph("Additional genes in genome", p_style)])
		
		data_present_pathways = data_present_pathways[:limit+3]
		numb_of_rows = len(data_present_pathways)
		TableStyle = []
		
		for  row_nmb in range(0, numb_of_rows):
			if row_nmb % 2 == 0:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), colors.white))
			else:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), "#EAFFFF"))
			TableStyle.append(("VALIGN", (0,row_nmb),(-1,row_nmb), "TOP"))
		
	
		table = Table(data_present_pathways, colWidths=(7*cm,2*cm,2*cm,2*cm,1.75*cm), hAlign="LEFT")
		TableStyle.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
		TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
		table.setStyle(TableStyle)	
		return table

	def checkOrthologsPresenceInQueryGenome(self, gene_set_orth_ids, query_genome_orth_id, groups):
		'''
		comments
		'''

		gene_cluster = {}
		

 		with open(groups, "r") as group:
			for line in group:
				line = line.strip().split(" ")
				del line[0]
				for line_element in line:			
					for gsoi in gene_set_orth_ids.keys():
						if line_element == gene_set_orth_ids[gsoi]:
							gene_cluster[gsoi] = line
				
					# if line_element in gene_set_orth_ids.values():
						# gene_cluster[gene_set_orth_ids[line_element]] = line
		
						
		for key in gene_cluster.keys():
			for key2 in gene_cluster.keys():
				if key in gene_cluster.keys():
					if key != key2:
						if gene_cluster[key] == gene_cluster[key2]:
							del gene_cluster[key2]
		
		return gene_cluster
				
	def getNotAnnotiatedOrthologous(self, not_annotiated_orthomcl_ids, groups):
		'''
		comments
		'''
		c = 0
		not_annotiated_gene_orthologous = {}
		with open(groups, "r") as group:
			for line in group:
				line = line.strip().split(" ")
				del line[0]
				
				for line_element in line:
					if line_element in not_annotiated_orthomcl_ids.keys():
						# print(line[line.index(line_element)])
						c+=1
						del line[line.index(line_element)]
						not_annotiated_gene_orthologous[line_element]  = line
				
				if len(not_annotiated_gene_orthologous.keys()) == len(not_annotiated_orthomcl_ids):
					break
		
		return not_annotiated_gene_orthologous

	def getQueryGenomeNotAnnotiatedOrthoMCLIdS(self, id_tags_dict, orthoMCL_query_genome_id):
		'''
		comments
		'''
		not_annotiated_orthomcl_id = {}
		c = 0
		# stop_loop_value = len(not_annotiated_genes) 
		
		for key in id_tags_dict.keys():
			if key.split("|")[0] == orthoMCL_query_genome_id:
				not_annotiated_orthomcl_id[key] = id_tags_dict[key]["proteinIdentifier"]
				c+=1
				
				
		# print(c)
		return not_annotiated_orthomcl_id
	
	def getGeneSetOrthoMCLIds(self, id_tags_dict, locus_tags, orthoMCL_query_genome_id):
		'''
		comments
		'''
		gene_set_orthomcl_ids = {}
		
		
		b = [key for key in id_tags_dict.keys()]
		
		orthmcl_id = "-"
		for lt in locus_tags:
			for key in id_tags_dict.keys():
				query_orth_id = key.split("|")[0]
				if query_orth_id == orthoMCL_query_genome_id:
					continue
				if id_tags_dict[key]["locus_tag"] == lt:
					orthmcl_id = key
					
			gene_set_orthomcl_ids[lt] = orthmcl_id
			orthmcl_id = "-"

		# for key in id_tags_dict.keys():
			# query_orth_id = key.split("|")[0]
			# if query_orth_id == orthoMCL_query_genome_id:
				# continue
			# for locus_tag in locus_tags:
				# if id_tags_dict[key]["locus_tag"] == locus_tag:
					# gene_set_orthomcl_ids[key] = locus_tag
			# if len(gene_set_orthomcl_ids.keys()) == len(locus_tag):
				# break
	
		return gene_set_orthomcl_ids
	
	def getIdTagsDict(self, IDs_to_tag):
		'''
		comments
		'''
		id_tags_dict = {}
		p = "locus_tag\|(.*)\|gi\|([\d.]+)\|ref\|([\d\w_.]+)\|(.*)\[(.*)\]"
		# p = "locus_tag\|(.*)\|gi\|([\d]+)\|ref\|([\d\w_.]+)\|(.*)\[(.*)\]"    1 version

		with open(IDs_to_tag, "r") as csv:
			for line in csv:
				line = [lineElement.strip() for lineElement in line.split("\t")]
				m = re.match(p, line[3])
				locus_tag = ""
				prot_id = ""
				product = ""
				organism = ""
				if m:
					locus_tag = m.group(1)
					prot_id = m.group(2)
					product = m.group(4).strip()
					organism = m.group(5)
				
				
				id_tags_dict["{}|{}".format(line[0],line[1])] = {"proteinIdentifier":prot_id, "product":product, "organism":
				organism, "locus_tag":
				locus_tag}
				

		return id_tags_dict
		
	def getTableDescription(self, text):
		'''
		comments
		'''
	
		ps = ParagraphStyle("my_ps")
		ps.fontSize = 9
		table_des_paragraph = Paragraph("{}".format(text), ps)
		
		return table_des_paragraph
		
	def writeTableToFile(self, not_annotiated_gene_orthologous, id_tags_dict):
		'''
		comments
		'''
		# print(id_tags_dict)
		# max_row = max([len(list) for list in not_annotiated_gene_orthologous.values()])
		
		custom_style = self.styles["Normal"]
		custom_style.leading = 9
	
		data = []
		for key in not_annotiated_gene_orthologous.keys():
			row = []
			orthologs = ""
			for orth in not_annotiated_gene_orthologous[key]:
				if str(id_tags_dict[orth]["product"]) == "":
					continue
				else:	
					orthologs += "- " + id_tags_dict[orth]["product"] + "<br />"
			row.append(Paragraph(id_tags_dict[key]["product"],custom_style))
			row.append(Paragraph(orthologs, custom_style))
			data.append(row)
		
		table = Table(data, colWidths=(8*cm, 8*cm), hAlign="LEFT")
		table.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
							('BOX', (0,0), (-1,-1), 0.25, colors.black),
							('LEADING', (0,0), (-1,-1), 0.25, 9)]))
		pdftable = PDFtable("genomes_ortho_table.pdf", table)

		pdftable.addTableElement()
		pdftable.savePDF()
		
	def getProteinTranslationTable(self, ptt_folder):
		'''
		comments
		'''
		# print(ptt_folder)
		
		PTTFilesPaths = os.listdir(ptt_folder)
		
		pttFileContent = {}
		for ptt_file in PTTFilesPaths:
			with open(ptt_folder + ptt_file, "r") as ptt:
				reader = csv.reader(ptt, delimiter='\t')
				for geneInfo in reader:
					if len(geneInfo) == 9:
						# ['Location', 'Strand', 'Length', 'PID', 'Gene', 'Synonym', 'Code', 'COG', 'Product']
						row = {}
						row["location"] = geneInfo[0]
						# print(geneInfo[5])
						row["strand"] = geneInfo[1]
						row["length"] = geneInfo[2]
						row["pid"] = geneInfo[3]
						row["gene"] = geneInfo[4]
						row["synonym"] = geneInfo[5]
						row["code"] = geneInfo[6]
						row["cog"] = geneInfo[7]
						row["product"] = geneInfo[8]
						pttFileContent[geneInfo[5]] = row
			# break
		return pttFileContent

	def pathwayPresenceInQueryGenome(self, gene_cluster, query_genome_orth_id):
		
		result_dict = {}
		genes_in_query_org = 0
		additional_genes_in_query_org = 0
		per_genes_in_query_org = 0
		all_meta_genes = len(gene_cluster.keys())
		result_dict["all_meta_genes"] = str(all_meta_genes)
		for key in gene_cluster.keys():
			for orth in gene_cluster[key]:
				org_code = orth.split("|")[0]
				if org_code == query_genome_orth_id:
					genes_in_query_org+=1
					
		
		if genes_in_query_org > all_meta_genes:
			additional_genes_in_query_org = genes_in_query_org - all_meta_genes
			genes_in_query_org = all_meta_genes
		
		result_dict["genes_in_query_org"] = genes_in_query_org
		result_dict["additional_genes_in_query_org"] = additional_genes_in_query_org
		
		
		if genes_in_query_org != 0:
			per_genes_in_query_org = round((genes_in_query_org/all_meta_genes), 2)
	
		result_dict["per_genes_in_query_org"] = per_genes_in_query_org
		
		
		return result_dict
		
	def addBgTable(self, numb_of_rows):
		
		TableStyle = []
		for  row_nmb in range(0, numb_of_rows):
			# print(row_nmb)
			if row_nmb % 2 == 0:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), colors.white))
			else:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), "#EAFFFF"))
			TableStyle.append(("VALIGN", (0,row_nmb),(-1,row_nmb), "TOP"))
		return TableStyle	
		
	def addElements(self, element):
		'''
		comments
		'''
		self.elements.append(element)
		
	