

from Bio import SeqIO
from reportlab.platypus import Paragraph, Table, TableStyle
from Bio.SeqUtils import CodonUsage 
class Basic_genome_property:
	'''
	comments
	'''
	def __init__(self, genbank, fasta):
		self.genbank = genbank
		self.fasta = fasta
		self.genbank_stat_table = None
		self.codon_usage_table = None
	
	def make_codon_usage(self, genbank, fasta):
		'''
		comments
		'''
		table = None
		codon_count_orf_list = []
		
		genome_orfs_file = self.writeORFGenomeFile(self.genbank, self.fasta)
		
		genome_codon_usage = self.calculateCodonUsage(genome_orfs_file)

		data = []
		row_value = []
		row_length = 8
		for aminoacid_codon_fam in genome_codon_usage.keys():
			
			codon_fam_freq = "<font size=8>"
			for codon in genome_codon_usage[aminoacid_codon_fam].keys():
				codon_fam_freq = codon_fam_freq + codon + "  " + str(genome_codon_usage[aminoacid_codon_fam][codon]) + "<br />"
				
			codon_fam_freq = codon_fam_freq + "</font>"
			
			aminoacid_codon_freq = "<font size=8>{}</font>".format(aminoacid_codon_fam) + "<br /><br />" + codon_fam_freq
			
			p = Paragraph(aminoacid_codon_freq, self.styles["Normal"])
			
			row_value.append(p)
			
			
			if genome_codon_usage.keys().index(aminoacid_codon_fam) == len(genome_codon_usage.keys()) - 1:
				for i in range(0, row_length-2):
					row_value.append("")
	
			if len(row_value) == row_length:
				data.append(row_value)	
				row_value = []
		
		
		TableStyle = []
		
		numb_of_rows = len(data)
		for  row_nmb in range(0, numb_of_rows):
			
			if row_nmb % 2 == 0:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), colors.white))
			else:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), "#EAFFFF"))
			TableStyle.append(("VALIGN", (0,row_nmb),(-1,row_nmb), "TOP"))
		
		
		table = Table(data, hAlign="LEFT")
		TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
		TableStyle.append(('VALIGN',(0,0),(-1,-1),'TOP'))
		TableStyle.append(("INNERGRID", (2,-1),(-1,-1), 0.25, colors.white))
		table.setStyle(TableStyle)
		
		make_codon_usage = table
	
	def make_genbank_stats_table(self):
		'''
		comments
		'''
		
		fasta_genome = SeqIO.read(self.fasta, "fasta")
		gc_per = round(((fasta_genome.seq.count("C") + fasta_genome.seq.count("G")) / len(fasta_genome.seq))*100, 1)
		
		data = []
		data.append(["Genome size", Paragraph(str(self.genbank.getGenomeSize()) + " bp", self.styles["Normal"])])
		data.append(["Number of genes", self.genbank.getNumberOfGenes()])
		data.append(["Average gene length", Paragraph(str(self.genbank.getAverageGeneLength()) + " bp", self.styles["Normal"])])
		data.append(["Average intergenic distance", Paragraph(str(self.genbank.getIntergenicRegion()) + " bp", self.styles["Normal"])])
		data.append(["GC percentage", Paragraph(str(gc_per) + " %", self.styles["Normal"])])
		
		numb_of_rows = len(data)
		TableStyle = []
		for  row_nmb in range(0, numb_of_rows):
			# print(row_nmb)
			if row_nmb % 2 == 0:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), colors.white))
			else:
				TableStyle.append(("BACKGROUND", (0,row_nmb),(-1,row_nmb), "#EAFFFF"))
			TableStyle.append(("VALIGN", (0,row_nmb),(-1,row_nmb), "TOP"))
		table = Table(data, 2*[5*cm], hAlign="LEFT")
		TableStyle.append(('INNERGRID', (0,0), (-1,-1), 0.25, colors.black))
		TableStyle.append(('BOX', (0,0), (-1,-1), 0.25, colors.black))
		
		table.setStyle(TableStyle)
		
		self.genbank_stat_table =  table
	
	def calculateCodonUsage(self, genome_orfs_file):
		'''
		comments
		'''
		
		sc = CodonUsage.SynonymousCodons
		codon_usage_genome = {}
		
		cai = CodonUsage.CodonAdaptationIndex()
		cai._count_codons(genome_orfs_file)
		# print(cai.codon_count)
		# print(sc)
		
		
		for aminoacid in sc.keys():
			codon_counts = {}
			for codon in sc[aminoacid]:
				codon_counts[codon] = cai.codon_count[codon]
			codon_usage_genome[aminoacid] = codon_counts
		
		codon_usage_percentage = {}
		# calculate codon frequency
		for aminoacid in codon_usage_genome.keys():
			total_codons = sum(codon_usage_genome[aminoacid].values())
			codon_distribution = {}
			codons_usages = ""
			
			for codon in codon_usage_genome[aminoacid].keys():
				percentage = 0
				if codon_usage_genome[aminoacid][codon] != 0:
					percentage = round((codon_usage_genome[aminoacid][codon] / total_codons)*100)
				codon_distribution[codon]  = percentage
			codon_usage_percentage[aminoacid] = codon_distribution
		
		return codon_usage_percentage
	
	def writeORFGenomeFile(self, genbank, fasta):
		'''
		comments
		'''
		fasta_genome = SeqIO.read(self.fasta, "fasta")
		genome_orfs_file = "orfs_genomes.fasta"
		orf_header = ""
		with open(genome_orfs_file, "w") as file:			
			
			
			for feature in self.genbank.getGenes():
				# print(dir(feature.location))
				
				# print(feature.location.end)
				if "locus_tag" in feature.qualifiers.keys():
					orf_header = feature.qualifiers["locus_tag"][0]
				
				else:
					
					orf_header = "{}...{}".format(feature.location.start, feature.location.end)
	
				orf = fasta_genome.seq[feature.location.start:feature.location.end]
				file.write(">"+orf_header+"\n")
				file.write(str(orf)+"\n")
		
		
		return genome_orfs_file
