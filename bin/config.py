
absolute_path = "/home/aponnu/Ashvin_graduation_project/"
output = absolute_path + "output/"

pdfName = output + "l_plantarum_WCFS1_report.pdf"
complete_gc = output + "l_plantarum_WCFS1_report_GC.pdf"
complete_cai = output + "l_plantarum_WCFS1_report_CAI.pdf"
complete_orthologs = output + "l_plantarum_WCFS1_report_orthologs.pdf"
complete_pathways = output + "l_plantarum_WCFS1_report_pathways.pdf"
orthoMCL_query_genome_id = "0007"


genbank =  absolute_path + "data/lactobacillus_plantarum_WCFS1_RAST.gbk"
fasta = absolute_path + "data/lactobacillus_plantarum_WCFS1.fasta"
pathway_geneset_folder = absolute_path + "data/pathwayGeneSets/"
pttFolder = absolute_path + "data/ptt/" 

IDs_to_tag = absolute_path + "data/IDs_to_tags.txt"
groups = absolute_path + "data/groups.txt"

pathwayCode2PathwayName = absolute_path + "data/pathwayCode2pathwayname.txt"
logo = absolute_path + "data/images/Logo_NIZO_CORPORATE_RGB_JPG.jpg"
circulair_genome_image = absolute_path + 'data/images/cover_image.png'

intro_text = absolute_path + "data/reportText/report_introduction.txt"
M_and_M_text = absolute_path + "data/reportText/report_materials_and_methods.txt"
genomic_properties_txt = absolute_path + "data/reportText/report_genomic_properties.txt"
codon_usage_txt = absolute_path + "data/reportText/report_codon_usage.txt"
genes_with_bias_codon_usage_txt = absolute_path + "/data/reportText/report_genes_with_bias_codon_usage.txt"
orthologous_genes_txt = absolute_path + "data/reportText/report_orthologous_genes.txt"
genes_with_significant_GC_percentage = absolute_path + "data/reportText/report_GC_percentage.txt"
metabolic_pathway_text = absolute_path + "data/reportText/report_metabolic_pathways.txt"

cover = True
mm = True
genomic_properties = True
diff_codon_usage = True
diff_gc_content = True
hypothetical_annotation = True
metabolic_pathways = True
build = True