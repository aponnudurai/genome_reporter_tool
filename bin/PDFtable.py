from decimal import Decimal
from lxml import etree, objectify

from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, mm, cm 
from reportlab.pdfgen import canvas
from MyDocTemplate import MyDocTemplate
from reportlab.platypus import Paragraph, Table, TableStyle
from  reportlab.platypus.frames import Frame

class PDFtable(object):
	
	def __init__(self, pdf_file):
		"""Constructor"""
        # self.xml_file = xml_file
		self.pdf_file = pdf_file
		self.story = []
		self.doc = None
        
        # self.xml_obj = self.getXMLObject()
        
   
	def coord(self, x, y, unit=1):
		"""
		# http://stackoverflow.com/questions/4726011/wrap-text-in-a-table-reportlab
		Helper class to help position flowables in Canvas objects
		"""
		x, y = x * unit, self.height -  y * unit
		return x, y  

		
	def addTableElement(self, table):
		"""
		Add table element
		"""
		self.doc = MyDocTemplate(self.pdf_file)
		styles = getSampleStyleSheet()
		self.story.append(table)


	def savePDF(self):
		"""
		Save the PDF to disk
		"""
		self.doc.multiBuild(self.story)
		
		
		