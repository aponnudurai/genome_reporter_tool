
from  reportlab.platypus.doctemplate import PageTemplate, BaseDocTemplate
from reportlab.lib.pagesizes import letter, A4
from  reportlab.lib.styles import ParagraphStyle as PS
from  reportlab.platypus import PageBreak, SimpleDocTemplate
from  reportlab.platypus.paragraph import Paragraph
from  reportlab.platypus.doctemplate import PageTemplate, BaseDocTemplate
from  reportlab.platypus.tableofcontents import TableOfContents
from  reportlab.platypus.frames import Frame
from  reportlab.lib.units import cm, inch, mm
from reportlab.lib.styles import getSampleStyleSheet


class MyDocTemplate(BaseDocTemplate):
	def __init__(self, filename, **kw):
		self.allowSplitting = 0
		apply(BaseDocTemplate.__init__, (self, filename), kw)
		cover = PageTemplate('cover', [Frame(1.5*cm, 2.5*cm, 18.0*cm, 25*cm, leftPadding=3, rightPadding=3, id='F1', topPadding=5,showBoundary=0)])
		template = PageTemplate('normal', [Frame(2.5*cm, 2.5*cm, 15*cm, 25*cm, id='F1', showBoundary=0, rightPadding=3)], onPage=self.footer)
		self.addPageTemplates([cover, template])
		

# Entries to the table of contents can be done either manually by
# calling the addEntry method on the TableOfContents object or automatically
# by sending a 'TOCEntry' notification in the afterFlowable method of
# the DocTemplate you are using. The data to be passed to notify is a list
# of three or four items countaining a level number, the entry text, the page
# number and an optional destination key which the entry should point to.
# This list will usually be created in a document template's method like
# afterFlowable(), making notification calls using the notify() method
# with appropriate data.
	def afterFlowable(self, flowable):
       
		# "Registers TOC entries."
		
		if flowable.__class__.__name__ == 'Paragraph':
			text = flowable.getPlainText()
			style = flowable.style.name
			if style == 'Heading1':
				level = 0
			elif style == 'Heading2':
				level = 1
			else:
				return
			E = [level, text, self.page]
			#if we have a bookmark name append that to our notify data
			bn = getattr(flowable,'_bookmarkName',None)
			if bn is not None: E.append(bn)
			self.notify('TOCEntry', tuple(E))
	
	def footer(self, canvas, doc):
		
		canvas.saveState()
		p = PS("my_style")
		p.alignment = 4
		p.fontSize = 9
		
		
		# footer
		Pfooter = Paragraph("Kernhemseweg 2, 6718 ZB Ede",p)
		w, h = Pfooter.wrap(doc.width, doc.topMargin)
		Pfooter.drawOn(canvas, doc.leftMargin, h)
		
		# header
		pHeader = Paragraph("NIZO Food Researcher",p)
		yHeader = doc.pagesize[1]-h*2
		wHeader, hHeader = pHeader.wrap(doc.width, doc.topMargin)
		pHeader.drawOn(canvas, doc.leftMargin, yHeader)
		
		# page number
		page_num = canvas.getPageNumber()
		text = "Page {}".format(page_num)
		pageNumb = Paragraph(text,p)
		wPageNumb, hPageNumbe = pageNumb.wrap(doc.width, doc.topMargin)
		xPos = doc.width + doc.rightMargin
		pageNumb.drawOn(canvas, xPos, hPageNumbe)		
		
		canvas.restoreState()
