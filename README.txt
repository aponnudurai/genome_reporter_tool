


How to run:
	
	- When new OrthoMCL results are used, change the "orthoMCL_query_genome_id". Set the OrthoMCL 
	id for the query genome. If no OrthoMCL results are used proceed to the next step.
	
	- Change "absolute_path" variable. Set the absolute path to the Ashvin_graduation_project folder.
	
	- python ./bin/program_reporter.py

	

